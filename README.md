## Papers to read:

#### Source separation:
- [ ] Linger, Pecka, Leibold, Grothe: A novel concept for dynamic adjustment of auditory space
- [ ] Eddins, Ozmeral, Eddins: How aging impacts the encoding of binaural cues and the perception of auditory space
- [ ] Oyamada, Kameoka, et al.: Generative adversarial network-based approach tosignal reconstruction from magnitude spectrogram
- [ ] Q Wang, H Muckenhirn, K Wilson (google): VoiceFilter: Targeted Voice Separation by Speaker-Conditioned Spectrogram Masking

##### Metrics/Loss:
- [x] E. Vincent, et al.: Performance Measurement in Blind Audio Source Separation 
      ([link](http://recherche.ircam.fr/equipes/analyse-synthese/vincent/vincent_IEEE04.pdf))
    - Signal to distortion ratio: $`\text{SDR} := \dfrac{||s_{target}||^2}{|| e_{interf} + e_{noise} + e_{artif} ||}`$
    - $`s_{target} := P_{s_j} \hat{s}_j = \langle\hat{s}_j, s_j\rangle s_j / ||s_j||^2`$
    - $`e_{interf} := P_{\bold s} \hat{s}_j - P_{s_j} \hat{s}_j`$
    - $`e_{noise} := P_{\bold s,n} \hat{s}_j - P_{\bold s} \hat{s}_j`$
    - $`e_{artif} := \hat{s}_j - P_{\bold s,n} \hat{s}_j`$
    - $`P_{s_j} := \prod{\{s_j\}}`$
    - $`P_{\bold s} := \prod{\{(s_j')_{1\le j'\le n}\}}`$
    - $`P_{\bold s,n} := \prod{\{(s_j')_{1\le j'\le n}, (n_i')_{1\le i\le m}\}}`$
- [x] Choi, et al.: Phase-Aware Speech Enhancement with Deep Complex U-Net (see below)
- [x] J. Kim, et al.: End-to-End Multi-Task Denoising forJoint SDR and PESQ Optimization ([link](https://arxiv.org/pdf/1901.09146.pdf))
- [x] A. Hines, et al.: ViSQOL: an objective speech quality model ([link](https://asmp-eurasipjournals.springeropen.com/articles/10.1186/s13636-015-0054-9)) ([code](https://sites.google.com/a/tcd.ie/sigmedia/))
    - Time alignment
    - Patch warping
    - Similarity comparison using Neurogram Similarity Index Measure (NSIM, [link](https://arrow.dit.ie/cgi/viewcontent.cgi?article=1043&context=scschcomart)) (a simplified version of structural similarity index (SSIM))
- [x] Martin-Donas, et al.: A Deep Learning Loss Function Based on the Perceptual Evaluation of the Speech Quality
    - Differentiable pesq loss similar to J. Kim, et al.: End-to-End Multi-Task Denoising forJoint SDR and PESQ Optimization 
- [x] J. Le Roux, et al.: SDR – HALF-BAKED OR WELL DONE?
    - Scale independent SDR metric; faster to compute; differentiable
    - $`\text{SI-SDR} = 10\log_{10}(\dfrac{||e_{\text{target}}||^2}{||e_{\text{res}}||^2})`$
    - $`\text{SI-SDR} = 10\log_{10}(\dfrac{||\frac{s^T\hat{s}}{||s||^2}s||^2}{||\frac{s^T\hat{s}}{||s||^2}s - \hat{s}||^2})`$
- [x] Barron, Jonathan T.: A general and adaptive robust loss function ([link](https://arxiv.org/pdf/1701.03077.pdf)) ([code](https://github.com/jonbarron/robust_loss_pytorch))
    - General, parametric loss of e.g. L2, pseudo-Huber, Cauchy, Geman-McClure and Welsch loss
    - $`f(x, \alpha, c) = \dfrac{|\alpha-2|}{\alpha} \left(\left(\dfrac{(x/c)^2}{|\alpha-2|}+1 \right)^{\alpha/2}-1 \right)`$, where $`\alpha \in \R`$ is a shape parameter that controls the robustness and $`c`$ a scale parameter that controlsthe size of the loss’s quadratic bowl near $`x= 0`$.
    - Gradient magnitude can be bounded
    - In practice negative log-likelihood (NLL) of the PDF of the loss is used
    - $`\alpha`$ can be made parametric by penalizing a low $`\alpha`$. I.e. a low $`\alpha`$ does not penalize outliers, thus decreasing $`\alpha`$ increases the loss (rather NLL) for inliners.
- [x] Yun Liu, et al.: Investigation of Cost Function for Supervised Monaural Speech Separation ([link](https://www.researchgate.net/publication/335829737_Investigation_of_Cost_Function_for_Supervised_Monaural_Speech_Separation))
    - Comparison of MSE and Divergance based cost functions like Kullback-Leibler (KL) divergence
    - Best w.r.t. STOI and PESQ was Jensen-Shannon divergence, which is a special symmetrized and smoothed version of the KL divergence
    - Best w.r.t. SDR was rGKL, where GKL is the generalized form of the KL divergence and rGKL is the reverse version of the GKL

#### Beamforming
- [x] Xiao, Xiong, et al.: Deep beamforming networks for multi-channel speech recognition ([link](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7472778))
    - Nice, simple paper that combines two networks
        1. Beamforming DNN
            - Gets GCC-Phat input features (Time Delay) from simulated data (GCC: generalized cross correlation between microphones)
            - For 8 microphones, there are $`C_2^8 = 28`$ microphone pairs. Depending on sr, overlap and microphone distance, the number of (center) elements of the GCC featurs has to be chosen.
            - Ouputs a complex value for each frequency and microphone. Real and imaginary are predicted independantly.
        2. Accoustiv model: In this case ASR
- [x] Pfeifenberger, Lukas, Matthias Zöhrer, and Franz Pernkopf: Deep Complex-valued Neural Beamformers ([link](https://ieeexplore.ieee.org/iel7/8671773/8682151/08683517.pdf))
    - No MVDR or GEV Beamformer, just simple filter-and-sum beamformer for each time-frequency bin
    - I.e. the model predicts a beamforming matrix W(t) of shape M x K, where K are freq bins, and M are the microphones
    - Uses complex valued lstms and fcs
- [x] Cauchi, Benjamin, et al.: Combination of MVDR beamforming and single-channel spectral processing for enhancing noisy and reverberant speech ([link](https://asp-eurasipjournals.springeropen.com/track/pdf/10.1186/s13634-015-0242-x))
    - Article with extensive overview and formulars
    - Old-school approach, no deep learning
- [x] Wang, Zhong-Qiu, and DeLiang Wang: All-NeuralMulti-Channel Speech Enhancement ([link](https://www.isca-speech.org/archive/Interspeech_2018/pdfs/1664.pdf))
    - 1: Single channel noise reduction on each microphone channel with BLSTM
        - Target is IRM of oracle beamformed signals
    - 2.1: Acoustic Beamforming -> Spectral feature extraction
        - Minimum Variance Distortionless Response (MVDR)
        - Estimation of speech and noise covariance matrices using the IRMs
        - Transfer between two microphone channels is the principal eigenvector of the speech covariance matrix
            -> Beamformer matrix can be construced using noise covariance matrix and transfere function 
    - 2.2: Robust TDOA Estimation -> Spatial feature extraction
        - Time delay of arrival is computed at each frequency using eigendecomposition
        - Given a set of potential time delays find a time delay that maximizes the similarity of estimated phase delay and the potential time delay
    - 3: BLSTM Multichannel separation based on IRM
- [x] Kristina Tesch, Robert Rehr, Timo Gerkmann: On Nonlinear Spatial Filtering in Multichannel Speech Enhancement ([link](https://www.isca-speech.org/archive/Interspeech_2019/pdfs/2751.pdf))
    - Comparison of MVDR Beamformer and modified beamformer using an optimal non-linear spatial filter that minimizes the minimum MSE
    - $`T^m_{MVDR}(\textbf{y}) = \dfrac{\textbf{d}^H\boldsymbol{\Phi}_n^{-1}\textbf{y}}{\textbf{d}^H\boldsymbol{\Phi}_n^{-1}\textbf{d}}`$, where $`\textbf{y}`$ is the noisy observation, $`\boldsymbol{\Phi}_n`$ the correlation/covariance matrix of the noise $`\textbf{N}`$ and $`\textbf{d} \in \mathbb{C}`$ a frequency dependent vector.
    - **Question:** What is $`\textbf{d}`$ exactly?
- [x] T. Ochiai et al.: Beam-TasNet: Time-domain audio separation network meets frequency-domain beamformer ([link](https://arxiv.org/pdf/1212.6080))
    - Apply Tasnet on each noisy channel
    - Afterwards transform into TF domain and compute traditional MVDR beamformer weights
    - Compute spacial covariance matrices based on tasnet output; apply weights
        on unprocessed noisy input
    - Align tasnet channels using autocorrelation

#### Speech enhancement generally
- [ ] Zhang et al.: OBJECTIVE COMPARISON OF SPEECH ENHANCEMENT ALGORITHMS WITHHEARING LOSS SIMULATION
- [ ] Y. Liu et al.: Supervised Speech Enhancement with Real Spectrum Approximation
- [x] K. Tan et al.: REAL-TIME SPEECH ENHANCEMENT USING AN EFFICIENT CONVOLUTIONALRECURRENT NETWORK FOR DUAL-MICROPHONE MOBILE PHONES IN CLOSE-TALKSCENARIOS
    - CRNN -> TF-Mask * Nosy Mag -> (noisy phase -> Resynthesizer)
    - Resynthesizer: Training target is phase sensitive mask (PSM)
    - $` \text{PSM}(t,f) = Re\Big\{ \dfrac{|S_1(t,f)|e^{j\theta_{s_1}}}{|Y_1(t,f)|e^{j\theta_{y_1}}} \Big\} = \dfrac{|S_1(t,f)|}{|Y_1(t,f)|} \cos (\theta_{s_1} - \theta_{y_1}) `$

##### Phase aware:
- [x] Zheng, Zhang: Phase-Aware Speech Enhancement Based on Deep Neural Networks
    - Jointly optimize STFT magnitude and the phase based representation, IFD
    - IFD: Instantaneous frequency deviation: 
        - IF: Principal values (range [-pi, pi]) of the derivative of the phase in time axis
        - IFD: Deviation of each frequency bin to its center frequency
- [ ] Williamson: MONAURAL SPEECH SEPARATION USING A PHASE-AWARE DEEP DENOISING AUTOENCODER
- [ ] Roux et al.: Phasebook and Friends:Leveraging discrete representationsfor source separation
- [ ] Z.Wang et al.: DEEP LEARNING BASED PHASE RECONSTRUCTION FOR SPEAKER SEPARATION: A TRIGONOMETRIC PERSPECTIVE

#### Complex
- [x] Trabelsi, et al.: Deep Complex Networks
    - Question Batch normalization: How is V positive semi definite? $`V_{ri} = Cov(Re(x), Im(x))`$ could be negative, right?
- [x] Choi, et al.: Phase-Aware Speech Enhancement with Deep Complex U-Net
    - Complex Masking: $`\hat{Y}_{t,f} = \hat{M}_{t,f} \cdot X_{t,f} = \bigl|\hat{M}_{t,f}\bigl|\cdot\bigl|X_{t,f}\bigl|\cdot e^{i(\theta_{\hat{M}_{t,f}} + \theta_{X_{t,f}})}`$
    - Weighted SDR loss: $`loss_{wSDR}(x,y,\hat{y}) := \alpha loss_{SDR}(y,\hat{y}) + (1-\alpha) loss_{SDR}(z,\hat{z})`$,
      where $`loss_{SDR}(y,\hat{y}) := -\sqrt(\dfrac{loss_{Ven}}{||y||^2}) = -\dfrac{<y,\hat{y}>}{||y|| ||\hat{y}||}`$
- [x] Hayakawa, et al.: Applying Complex-Valued Neural Networks toAcoustic Modeling for Speech Recognition
    - Other batch norm: Only scale by 1/(mean mag) to not change the phase 
- [x] Virtue et a;.: Better than Real: Complex-valued Neural Nets for MRI Fingerprinting
    - Phase sensitive activation: complex cardioid
- [ ] M. Wolter et al.: Complex Gated Recurrent Neural Networks
- [ ] Parcollet et al.: BIDIRECTIONAL QUATERNION LONG SHORT-TERM MEMORYRECURRENT NEURAL NETWORKS FOR SPEECH RECOGNITION
- [ ] Tan et al.: COMPLEX SPECTRAL MAPPING WITH A CONVOLUTIONAL RECURRENT NETWORKFOR MONAURAL SPEECH ENHANCEMENT
- [x] Ouyang et al.: A FULLY CONVOLUTIONAL NEURAL NETWORK FOR COMPLEX SPECTROGRAMPROCESSING IN SPEECH ENHANCEMENT
    - Small 50k parameter conv net with complex input (251 x 13); i.e. 500 pt FFT, 13 frames at fs=16k corresponfs to about 400ms
    - Frequency dilitated 2d convs; overall delay about 200 ms

#### Features:
- [ ] Xu, Du, Dai, Lee: Dynamic noise aware training for speech enhancement based on deep neural networks
- [ ] Rehr, Gerkmann: AN ANALYSIS OF NOISE-AWARE FEATURES IN COMBINATION WITH THE SIZE AND DIVERSITY OF TRAINING DATA FOR DNN-BASED SPEECH ENHANCEMENT
- [ ] Rehr, Gerkmann: Robust DNN-Based Speech Enhancement with Limited Training Data
- [x] O. Viikki, et al.: Recursive feature vector normalization approach for robust speech recognition in noise

#### Filterbanks:
- [x] Manuel Pariente et al.: Filterbank design for end-to-end speech separation ([paper](https://arxiv.org/pdf/1910.10400.pdf), [code](https://github.com/mpariente/asteroid))
    - Filterbank for Conv-Tasnet
    - Sinc filters inspired by SincNet (Ravanelli etal 2018),
        $`2f_w \text{sinc}(2\pi f_w n)\cos(2\pi f_C n)`$
    - Propose to add odd filters (to existing even filters) to have a complete
        basis, $`2f_w \text{sinc}(2\pi f_w n)\sin(2\pi f_c n)`$
    - As general case: Analyitc filters; Hilbert transform (quadrature phase
        shift) $`u_{analytic}(t)=u(t)+j\mathcal{H}`$
    - Intersting finding: For short window real/imag as input is beneficial, for
        larger windows, magnitude is sufficient
- [x] David Ditter, et al.: A Multi-Phase Gammatone Filterbank for Speech
    Separation Via Tasnet ([paper](https://arxiv.org/abs/1910.11615), [code](https://github.com/sp-uhh/mp-gtf))
    - TasNet learns a filterbank similar to frequency log scaled filterbank
    - Proposed deterministic gamma tone filterbank:
        $`\gamma(t)=at^{(t-1)}e^{-2\pi b t}\cos(2\pi f_c t+\Phi)`$
    - Multiple phases (2-5) per center frequency seem beneficial
    - Frequency scaled using ERB scale (similar to gamma tone scale)
    ![MP-GTF](img/Ditter_GTF.png)

#### Clustering
- [ ] S. Wang et al.: LOW-LATENCY DEEP CLUSTERING FOR SPEECH SEPARATION

#### Attention

- [ ] Wu et al. (2019): Pay Less Attention with Lightweight and Dynamic Convolutions
- [x] Vaswani A., et al.: Attention Is All You Need ([link](https://papers.nips.cc/paper/7181-attention-is-all-you-need.pdf))
    - Replace RNNs completely with self attention (+FC layers)
    - Proposed model is the Transformer architecture
        - Input embedding + positional encoding
          - -> Multihead attention -> Addnorm -> FC -> Addnorm
        - Output embedding + positional encoding
          - -> Multihead attention -> Addnorm -> FC -> Addnorm
        - -> Again Multihead Attention like above now with
          - -> Input for Keys and Values
          - -> Ouput for queries
        - -> Linear -> Softmax
- [x] Hao, Xiang, et al.: An Attention-based Neural Network Approach for Single Channel Speech Enhancement ([link](https://ieeexplore.ieee.org/iel7/8671773/8682151/08683169.pdf))
    - Stacked/Expanded LSTM Encoder
    - Attention on encoder output frames
        - casual dynamic attention: Use attention weights from all past frames
        - casual local attention: Use only n attention weights from the last n frames
- [x] Sukhbaatar S., et al.: Adaptive Attention Span in Transformers ([link](https://arxiv.org/pdf/1905.07799.pdf))
    - Modify the self attention layer of a sequential transformer network to use an adaptive span
    - Therefore, the outut vector $`s_{tr}`$ will be masked for the output vector $`A_{tr}`$ using a parameterized soft masking function $`m_z`$, where $`z`$ is a learnable real value.
    - An $`l_1`$ penalty of $`z`$ was added to the model loss
    - Dynamic version: Instead of learning $`z`$, an affine transform will be learned that outputs $`z`$
- [x] Nishikimi, Ryo, et al.: Automatic Singing Transcription Based on Encoder-decoder Recurrent Neural Networks with a Weakly-supervised Attention Mechanism. ([link](http://sap.ist.i.kyoto-u.ac.jp/members/yoshii/papers/icassp-2019-nishikimi.pdf))
    - Encoder with bidirectional LSTM transforms input $`[x_1 \dots x_T]`$ to hidden states $`[h_1 \dots h_T]`$
    - Attention-based decoder is unidirectional RNN
        - Attention of last recurrent state, hidden state and last attention
        - Attention score function, that computes the raw weights for attention is based on a convolution, $`e_{nt}`$ is computed by learned linear combination
- [x] Shen, Yu-Han, Ke-Xin He, and Wei-Qiang Zhang.: Learning How to Listen: A Temporal-Frequential Attention Model for Sound Event Detection ([link](https://arxiv.org/pdf/1810.11939.pdf))
    - Mel Spectrogram as Input
    - However no "real" attention: Only max from sigmoid activated FC, that get's normalized over all timesteps/frequencies afterwards. No Attention using keys, values and queries
    - Frequency attention produces a weight mask $`M_{t, f}`$ for the input spectrogram; basically learns a low pass
    - CRNN: Get's input of 'attention' weighted spectrogram
    - Time 'attention' gets CNN output
    - FC classifier for activity detection get's concatenated time attention and bi-gru output
- [x] Pham, Ngoc-Quan, et al.: Very Deep Self-Attention Networks for End-to-End Speech Recognition ([link](https://arxiv.org/pdf/1904.13377.pdf))
    - Transformer architecture
    - Encoder takes projected (fc layer on frequencies) audio features + positional encoding (sinusoidal)
    - Self attention with 512 dim input per time step and 1024 hidden dim
    - Decoder takes character embeddings + positional encoding as input
    - Use of stochastic layers on residual networks: Randomly selected layers are dropped during training; This results in only the residual connection remaining. The probability of dropping a layer during training rises for deeper layers. Similar to dropout the network output needs to be scaled during inference by the dropout probability.
    - Use of learning rate warmup $`lr = lr_{init} * d^{-0.5} \cdot \min(step^{-0.5}, step*warmup^{-1.5})`$, where $`d=512`$ model dim
- [x] Yang, Baosong, et al.: Convolutional Self-Attention Networks ([link](https://arxiv.org/pdf/1904.03107.pdf)):
    - Use of 1D or 2d conv instead of FC layers for computing key, values and queries
- [x] Chorowski, Jan K., et al.: Attention-based models for speech recognition ([link](http://papers.nips.cc/paper/5847-attention-based-models-for-speech-recognition.pdf))
    - Encoder multi layer BI-GRU (size=256): output is used as input representation $`h`$ of the attention
    - Generate FC, that takes the previous generator state $`s_{i-1}`$ and the current attention $`\alpha_i`$
    - Generator: Single layer GRU (size=256)
    - Attention, where $`\alpha_i \in \R^L`$ are the attention weights
      - Hybrid attention, where both, $`\alpha_{i-1}`$ (location based), and $`h_{j}`$ (content based) where used for the attention.
      - Therefore, k vectors $`f_{i,j} \in \R^k`$ are extracted from $`\alpha_{i-1}`$ for every position $`j`$ of the previous alignment $`\alpha_{i-1}`$, via convolution $`f_i = F * \alpha_{i-1}`$.
      - Smoothing by replacing the exponential function in the softmax that computes $`\alpha_{i,j}`$ by a bounded logistig sigmoid

#### Quantization:
- [ ] Aojun Zhou, Anbang Yao, Yiwen Guo, Lin Xu, Yurong Chen: Incremental Network Quantization: Towards Lossless CNNs with Low-Precision Weights ([link](https://arxiv.org/pdf/1702.03044))

### RNNs
- [x] Tao Lei, et al: Simple Recurrent Units for Highly Parallelizable Recurrence ([link](https://arxiv.org/pdf/1709.02755)) ([code](https://github.com/taolei87/sru))
- [x] Sak, Haşim, et al.: Fast and Accurate Recurrent Neural Network Acoustic Models for Speech Recognition ([link](https://arxiv.org/pdf/1507.06947.pdf))
    - Frame stacking: Concat multiple spectrogram frames, e.g. 8 into one feature vector/matrix; skip e.g. 3 frames forward for the next feature vector
- [x] Ravanelli, Mirco, et al.: Light gated recurrent units for speech recognition ([link](https://www.researchgate.net/profile/Mirco_Ravanelli/publication/324017338_Light_Gated_Recurrent_Units_for_Speech_Recognition/links/5ab9334945851515f5a0c442/Light-Gated-Recurrent-Units-for-Speech-Recognition.pdf))
    - Remove reset gate $`r`$ and change hidden activation to relu
    - This removes 1 weight matrix and bias and cuts down the number of
        parameters to 2xhidden size (GRU: 3xh_s, LSTM: 4x h_s) for the input-,
        and hidden weights, as well as biases.
- [x] Daiki Takeuchi, et al.: Real-time speech enhancement using equilibriated RNN ([link](https://arxiv.org/pdf/2002.05843.pdf))
    - Use skip connections in a simple rnn like structure
    - Multiple iterations (1-5) for each time steps, but no significant benefit
    ![ERNN Architecture](img/Takeuchi_ERNN.svg)


### LPC
- [ ] Valin, et al.: LPCNET: IMPROVING NEURAL SPEECH SYNTHESIS THROUGH LINEAR PREDICTION
    - Note to myself: Ask Mathias about the student project

### Pruning
- [ ] Frankle, Dziugaite, Roy, Carbin: Stabilizing the Lottery Ticket Hypothesis ([link](https://arxiv.org/pdf/1903.01611.pdf))

### Distillation Learning (Teacher-Student)
- [x] Hinton, Geoffrey, Oriol Vinyals, and Jeff Dean: Distilling the knowledge in a neural network ([link](https://arxiv.org/pdf/1503.02531))
    - Erstes Distillation learning paper
- [x] Subramanian, Aswin Shanmugam, Szu-Jui Chen, and Shinji Watanabe: Student-Teacher Learning for BLSTM Mask-based Speech Enhancement ([link](https://www.isca-speech.org/archive/Interspeech_2018/pdfs/2440.pdf))
    - Teacher gets beamformed signal as input; predicts binary mask
    - Student gets single channel noisy signal as input
    - Student loss is a combination of the soft targe output of the teacher and ideal binary masks of speech and noise
- [x] Meng, Zhong, et al.: Conditional Teacher-student Learning  ([link](https://ieeexplore.ieee.org/abstract/document/8683438))
    - Idea: If the teacher is "very" wrong use hard labels instead of soft teacher labels
- [x] L. Drude, et al.: Unsupervised Training of a Deep Clustering Model for Multichannel Blind Source Separation ([link](https://ieeexplore.ieee.org/abstract/document/8683520))
    - Deep Clustering approach
    - Teacher is GMM; Student is better than teacher in the end

### Misc
- [x] Zhang, Richard.: Making convolutional networks shift-invariant again ([link](https://arxiv.org/pdf/1904.11486.pdf)) ([code](https://github.com/adobe/antialiased-cnns))
    - Motivation: Stride in max-pool and in convolutions result in aliasing, i.e. they break the sampling theorem
    - From signal processing: Blur (low pass) before subsampling
    - Thus better than Conv(stride): Conv (without stride) -> blurpool (via conv) -> subsample with stride
    - Disadvantage: More operations for the same amount of learnable parameters
